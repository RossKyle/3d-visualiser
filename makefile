# TODO: make dirs if they don't exist
# TODO: clean

# *****************************************************
PROJNAME = 3DVisualizer

CC = g++
CFLAGS = -Wall -g
C++STD = -std=c++20
C++OPT = -O0
LIB = -lglfw -lGLEW -lGLU -lGL

OBJDIR = obj
SRCDIR = src
BINDIR = bin
INCLUDE = -I include
TARGET = $(BINDIR)/$(PROJNAME)

SOURCES = $(shell find $(SRCDIR) -type f -name *.cpp)
OBJECTS = $(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(SOURCES:.cpp=.o))
# ****************************************************

$(TARGET): $(OBJECTS)
	@echo "Linking project $(PROJNAME)"
	$(CC) $(CFLAGS) $^ -o $(TARGET) $(LIB) -fconcepts
	@echo "Executable created in $(BINDIR)"

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp 
	$(CC) $(CFLAGS) $(C++STD) $(C++OPT) $(INC) -c -o $@ $<

