//TODO: use c++20 modules instead of preprocessor includes
#include <glm/glm.hpp>

#include "../include/main.h"
#include "../include/tools/filetools.h"
#include "../include/tools/shadertools.h"
#include "../include/tools/stringtools.h"
#include <memory>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include <cstring>
#include <chrono>
#include <vector>

using namespace std;

constexpr auto vertexShaderFilepath = "src/shaders/standardVertexShader.glsl";
constexpr auto fragmentShaderFilepath = "src/shaders/standardFragmentShader.glsl";
constexpr auto pointCloudFilepath = "triangle.xyzrgb";
constexpr auto targetFrameRate = 60;
constexpr auto targetDeltaTime = 1.0f / targetFrameRate;
constexpr auto title = "CHANGE ME";

GLuint generateVertexBuffer()
{
    GLfloat vertices[] = {
        -.5f,   -.5f,   .0f,
         .5f,   -.5f,   .0f,
         .0f,    .5f,   .0f
    }; 

    GLfloat colors[] = {
        -1.0f,  .0f,    .0f,
        .0f,    1.0f,   .0f,
        .0f,    .0f,    1.0f
    }; 

    GLuint vboID[2];
    glGenBuffers(2, &vboID[0]);
    glBindBuffer(GL_ARRAY_BUFFER, vboID[1]);  
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    size_t entrySize = sizeof(GLfloat);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, entrySize, (void*)0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, entrySize, (void*)sizeof(vertices));

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
}

int createVertexArrayObject()
{
    // A vertex is a point on a polygon, it contains positions and other data (eg: colors)
    const glm::vec3 vertexArray[] = {
        glm::vec3( 0.0f,  0.5f, 0.0f),  // top center position
        glm::vec3( 1.0f,  0.0f, 0.0f),  // top center color (red)
        glm::vec3( 0.5f, -0.5f, 0.0f),  // bottom right
        glm::vec3( 0.0f,  1.0f, 0.0f),  // bottom right color (green)
        glm::vec3(-0.5f, -0.5f, 0.0f),  // bottom left
        glm::vec3( 0.0f,  0.0f, 1.0f),  // bottom left color (blue)
    };
    
    // Create a vertex array
    GLuint vertexArrayObject;
    glGenVertexArrays(1, &vertexArrayObject);
    glBindVertexArray(vertexArrayObject);
    
    // Upload Vertex Buffer to the GPU, keep a reference to it (vertexBufferObject)
    GLuint vertexBufferObject;
    glGenBuffers(1, &vertexBufferObject);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexArray), vertexArray, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(glm::vec3), (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(glm::vec3), (void*)sizeof(glm::vec3));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0); // VAO already stored the state we just defined, safe to unbind buffer
    glBindVertexArray(0); // Unbind to not modify the VAO
    
    return vertexArrayObject;
}

int main(int argc, char* argv[])
{
    initGLFW();
    const auto window = CreateWindow(800, 600, string_view{title});
    glfwMakeContextCurrent(window);
    initGLEW();

    int shaderProgramID = ShaderTools::createShaderProgram(vector<string_view>{vertexShaderFilepath, fragmentShaderFilepath});
    glUseProgram(shaderProgramID);
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    int vao = createVertexArrayObject();
    loop(*window, shaderProgramID, vao);

    return EXIT_SUCCESS;
}

void render(GLFWwindow& window, int shaderProgramID, int vao)
{
    glClear(GL_COLOR_BUFFER_BIT);
    
    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(0);
    
    glfwSwapBuffers(&window);
}

// TODO: "Free the physics" (https://gafferongames.com/post/fix_your_timestep/)
void loop(GLFWwindow& window, int shaderProgramID, int vao)
{
    auto previousTimePoint = chrono::high_resolution_clock::now();

    while(!glfwWindowShouldClose(&window))
    {
        auto newTimePoint = chrono::high_resolution_clock::now();
        float frameTime = (newTimePoint - previousTimePoint).count();
        previousTimePoint = newTimePoint;

        while (frameTime > .0f) {
            float deltaTime = min(frameTime, targetDeltaTime);
            frameTime -= deltaTime;

            update(window, deltaTime);
        }
        render(window, shaderProgramID, vao);
    }
    glfwTerminate();
}

void update(GLFWwindow& window, float deltaTime)
{
    glfwPollEvents();
    handleEvents(window);
}

void handleEvents(GLFWwindow& window)
{
    if (glfwGetKey(&window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(&window, true);
}

void initGLFW()
{
    if (!glfwInit()) {
        cerr << "Can't initialize GLFW" << endl;
        exit(EXIT_FAILURE);
    }
}

void initGLEW()
{
    glewExperimental = true;
    if (glewInit() != GLEW_OK) {
        cerr << "Failed to create GLEW" << endl;
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
}

GLFWwindow* CreateWindow(const short width, const short height, string_view title)
{
    GLFWwindow* window = glfwCreateWindow(width, height, title.data(), nullptr, nullptr);

    if (!window) {
        cerr << "Failed to create GLFW window" << endl;
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); 
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); 
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    glfwSetFramebufferSizeCallback(window, [](GLFWwindow* window, const int width, const int height){glViewport(0, 0, width, height);});

    return window;
}

//TODO: revisit this function for latest MacOS
void seputMacOS()
{
    #ifdef PLATFORM_OSX
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif
}
