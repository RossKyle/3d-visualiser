#include "../../include/tools/shadertools.h"
#include "../../include/tools/filetools.h"

#include <iostream>

using namespace std;

vector<string> ShaderTools::readShaderSourceCode(const strview_vector& shaderFilepaths)
{
    auto shaderSourceCode = vector<string>();
    shaderSourceCode.reserve(shaderFilepaths.size());
    
    for (auto& filepath : shaderFilepaths) {
        try {
            shaderSourceCode.push_back(FileTools::readTextFile(filepath));
        }
        catch (int n) {
            cerr << filepath << "not found." << endl;
            exit(EXIT_FAILURE);
        }
    }

    //TODO: make sure this can be RVO-ed in order to not copy all source code twice
    return shaderSourceCode;
}

int ShaderTools::compileShader(string_view shaderSourceCode, const int shaderType)
{
    int shader = glCreateShader(shaderType);
    auto cstrShaderSourceCode = shaderSourceCode.data();
    glShaderSource(shader, 1, &cstrShaderSourceCode, NULL);
    glCompileShader(shader);

    // TODO: validation should be in its own function
    int success = 0;
    char infoLog[512]{0};
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        cerr << "Failed to compile shader" << endl << infoLog << endl;
    }

    return shader;
}

int ShaderTools::linkShaderProgram(const vector<int>& shaderIDs)
{
    const int shaderProgram = glCreateProgram();

    attachShadersToProgram(shaderProgram, shaderIDs);
    glLinkProgram(shaderProgram);
    detachShadersToProgram(shaderProgram, shaderIDs);

    // TODO: validation should be in its own function
    int success = 0;
    char infoLog[512]{0};
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        cerr << "Failed to create shader program" << endl << infoLog << endl;
    }

    return shaderProgram;
}

vector<int> ShaderTools::compileShaders(const strview_vector& shaderSourceCode)
{
    return vector<int> {
        compileShader(shaderSourceCode[0]), 
        compileShader(shaderSourceCode[1], GL_FRAGMENT_SHADER)
    };
}

int ShaderTools::createShaderProgram(const strview_vector& shaderFilepaths)
{
    auto sourceCode = readShaderSourceCode(shaderFilepaths);
    strview_vector s(sourceCode.begin(), sourceCode.end());
    return linkShaderProgram(compileShaders(s));
}

void ShaderTools::attachShadersToProgram(const int shaderProgram, const vector<int>& shaderIDs)
{
    for (const int id : shaderIDs)
        glAttachShader(shaderProgram, id);
}

void ShaderTools::detachShadersToProgram(const int shaderProgram, const vector<int>& shaderIDs)
{
    for (const int id : shaderIDs)
        glDeleteShader(id);
}