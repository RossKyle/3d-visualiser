#include "../../include/tools/filetools.h"
#include <fstream>
#include <sstream>

using namespace std;

string FileTools::readTextFile(string_view filepath)
{
    ifstream file(filepath.data());
    stringstream buffer;

    if (!file)
        throw 5;

    buffer << file.rdbuf();
    
    return buffer.str();
}