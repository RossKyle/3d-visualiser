#include "../../include/tools/stringtools.h"
#include <sstream>

using namespace std;

vector<string> StringTools::getTokens(const string& source)
{
    vector<string> tokens;
    stringstream sstream(source);
    string token;

    while(getline(sstream, token, ' '))
        tokens.push_back(token);
    
    return tokens;
}

template <class T>
vector<T> convertStrings(const vector<string>& strings) 
{
    vector<T> values;
    values.reserve(strings.size());
    T value;

    for (const auto& str : strings)
    {
        from_chars(str.data(), str.data() + str.size(), value);
        values.push_back(value);
    }

    return values;
}