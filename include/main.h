#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string_view>

void initGLFW();
void initGLEW();
GLFWwindow* CreateWindow(const short width, const short height, std::string_view title);
void loop(GLFWwindow& window, int shaderProgram, int vao);
void update(GLFWwindow& window, float deltaTime);
void handleEvents(GLFWwindow& window);

void setupMacOS();
