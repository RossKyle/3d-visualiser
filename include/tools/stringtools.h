#pragma once

#include <string>
#include <vector>
#include <concepts>

class StringTools {
    public:
        static std::vector<std::string> getTokens(const std::string& source);
};