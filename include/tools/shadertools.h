#pragma once

#include <string>
#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

class ShaderTools {
    typedef std::vector<std::string_view> strview_vector;

    public:
        static std::vector<std::string> readShaderSourceCode(const strview_vector& shaderFilepaths);
        static int compileShader(std::string_view shaderSourceCode, const int shaderType = GL_VERTEX_SHADER);
        static std::vector<int> compileShaders(const strview_vector& shaderSourceCode);
        static int linkShaderProgram(const std::vector<int>& shaderIDs);
        static int createShaderProgram(const strview_vector& shaderFilepaths);

    private:
        static void attachShadersToProgram(const int shaderProgram, const std::vector<int>& shaderIDs);
        static void detachShadersToProgram(const int shaderProgram, const std::vector<int>& shaderIDs);
};