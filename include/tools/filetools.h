#pragma once 

#include <string>

class FileTools {
    public:
        static std::string readTextFile(std::string_view filepath);
};