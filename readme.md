# 3D Visualiser 

The current goal of this _OpenGL_ project is to visualize meshes and point clouds from a file.
Future goals include visualizing meshes and point clouds from a stream (e.g. depth camera) in _realtime_.

## Features

* Visualize meshes and point clouds from a file in a 3D space
* Viewer camera controls
* Call external program to generate viewable meshes from point clouds (e.g. neural network model)

## Building
### Using gmake (GNU make)

```shell
cd project_directory
make #call gmake on most Linux distributions
```

The `makefile` is handwritten to be minimal.

## Related projects

* (_not started_) Point Cloud to Mesh Neural Network

## Dependencies

* GLFW3
* GLEW

## Modern C++ uses

This program uses c++ language features up to `std=c++20`.

### C++ 11
* `auto`
* `constexpr`
* Lambda expressions

### C++ 17
* `std::string_view`

### C++ 20
* Concepts and constraints (requires `-fconcepts` build flag in _GCC 10+_)

## Rules

* Do not interleave VBO data.

    > How much interleaving attributes helps in rendering performance is not well understood. Profiling data are needed. Interleaved vertex data may take up more room than un-interleaved due to alignment needs.
    > https://www.khronos.org/opengl/wiki/Vertex_Specification_Best_Practices#Interleaving
